from algos import cubre_intervalos

pruebas = [
    [[0,3],[2,6],[3,4],[6,9]],
    [[5,6],[0,3],[2,6],[3,4],[6,9]],
    [[1,2],[3,4],[5,6],[7,8]],
    [],
    [[1,5],[7,10],[10,11]],
    [[1,6],[-3,5],[4,6],[5,6],[3,6],[7,10]]
]

def imprime_pruebas(intervalos):
    resultado = cubre_intervalos(intervalos)
    print(f'{intervalos} da {resultado}')

for intervalos in pruebas:
    imprime_pruebas(intervalos)
