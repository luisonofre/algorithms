# Algoritmia

## ¿Cómo realizar las pruebas?

Es necesario tener instalado `python 3`

Para los ejercicios `1` y `3` , hay pruebas unitarias en el archivo `algos_test.py`. Es necesario tener instalado `pytests`

```bas
pip3 install -U pytest
```

Y para ejecutar las pruebas:

```bas
pytest test_algos.py
```



En el caso del `ejercicio 2`, hay un archivo `main.py` el cual imprime algunos casos de prueba:

```bas
python3 main.py
```

