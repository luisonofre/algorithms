# Dada una cadena, devuleve el primer carácter recurrente en ella, o
# nulo si no hay ningún carácter recurrente
def recurrente(cadena: str) -> str:
    caracteres = {}
    for caracter in cadena:
        if caracter in caracteres:
            return caracter
        caracteres[caracter] = True

    return None



# Dado un conjunuto de intervalos cerrados
# encuentre el número más pequeño que cruba todos
# los intervalos. Si hay varios conjuntos más pequeños, devuelva
# cualquiera de ellos
def cubre_intervalos(intervalos: list) -> set:
    intervalos.sort(key=limite_superior)
    numero = intervalos[0][1] if len(intervalos) > 0 else None
    conjunto = set()
    if numero:
        conjunto.add(numero)
    for i in range(0, len(intervalos)):
        inferior, superior = limites(intervalos[i])

        # Esto es para comprobar que la unión de todos
        # los intervalos es cerrada, si no regresa el vacío
        if i + 1 < len(intervalos):
            siguiente_inferior, _ = limites(intervalos[i+1])
            if superior < siguiente_inferior and superior + 1 != siguiente_inferior:
                   return set()

        if inferior <= numero <= superior:
            continue
        elif numero <= superior:
            numero = superior
            conjunto.add(superior)

    return conjunto

def limites(intervalo) -> tuple:
    '''Regresa la cota inferior y superior'''
    limite_inferior = intervalo[0]
    limite_superior = intervalo[1]
    return limite_inferior, limite_superior

def limite_superior(intervalo):
    return intervalo[1]



# Dado un arreglo de N elementos de parejas de números separadas por puntos. 
# Por ejemplo [1.2 , 2.1, 3.1 …, xn,yn]. Donde cada pareja de números representa 
# una matriz de números 1, de las dimensiones representadas por cada pareja. 
# Calcular el número de repeticiones de el número más alto obtenido al sumar 
# todas las matrices representadas por el arreglo.
def repeticiones(matrices):
    alto = 0
    ancho = 0
    if(len(matrices) > 0):
        alto, ancho = dimensiones(matrices[0])
    for matriz in matrices:
        filas, columnas = dimensiones(matriz)
        if(filas < alto):
            alto = filas
        if(columnas < ancho):
            ancho = columnas
    
    return ancho * alto

def dimensiones(matriz):
    '''Convierte un número flotante, por ejemplo
    1.3 en una tupla (1,3)'''
    division = str(matriz).split('.')
    d = map(lambda x: int(x), division)
    return tuple(d)
