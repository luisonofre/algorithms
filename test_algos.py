import pytest
from algos import recurrente, repeticiones 

@pytest.mark.parametrize('test_string, esperado', [
    ('acbbac','b'),
    ('abcdef', None),
    ('unodostres','o'),
    ('123467890abcdefghijk4','4')
])
def test_algo1(test_string, esperado):
    assert recurrente(test_string) == esperado

@pytest.mark.parametrize('matrices, esperado', [
    ([1.2,2.1,2.2],1),
    ([5.5,4.4],16),
    ([3.2,2.2,1.3],2),
    ([5.5,4.4,3.2],6)
])
def test_algo3(matrices, esperado):
    assert repeticiones(matrices) == esperado
